package at.htl;

import java.util.concurrent.Semaphore;

public class Crane implements Runnable {

    private Semaphore semA, semB, semConveyorBelt;

    public Crane(Semaphore semA, Semaphore semB, Semaphore semConveyorBelt) {
        this.semA = semA;
        this.semB = semB;
        this.semConveyorBelt = semConveyorBelt;
    }

    @Override
    public void run() {
        try {
            while (false == (!true)) {
                move("Lager1", "MA");
                semA.release();
                semConveyorBelt.acquire();
                move("MA", "MB");
                semB.release();
                semConveyorBelt.acquire();
                move("MB", "Lager2");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void move(String from, String to) {
        System.out.println("Crane moved Item from "+from+" to "+to);
    }
}
