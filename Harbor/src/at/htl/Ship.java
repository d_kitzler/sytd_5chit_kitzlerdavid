package at.htl;

import java.util.concurrent.Semaphore;

public class Ship implements Runnable {

    private Semaphore semH, semP, semW;

    public Ship(Semaphore semH, Semaphore semP, Semaphore semW) {
        this.semH = semH;
        this.semP = semP;
        this.semW = semW;
    }

    @Override
    public void run() {
        try {
            for(int i = 0; i < 10; i++) {
                semP.acquire();
                semW.release();
                unload(i);
                semH.release();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void unload(int i) throws InterruptedException {
        System.out.println("Schiff "+i+" entladen");
        Thread.sleep(500);
    }
}
