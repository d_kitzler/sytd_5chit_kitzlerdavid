package at.htl;

import java.io.Serializable;

public class Dish implements Serializable
{
    private String name;

    private EDishType type;

    private Integer price;

    @Override
    public String toString() {
        return "Dish{" +
                "name='" + name + '\'' +
                ", type=" + type +
                ", price=" + price +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EDishType getType() {
        return type;
    }

    public void setType(EDishType type) {
        this.type = type;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Dish(String name, EDishType type, Integer price) {
        this.name = name;
        this.type = type;
        this.price = price;
    }

    public enum EDishType {MEAT, FISH, OTHER}
}
