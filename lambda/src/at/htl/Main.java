package at.htl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class Main {

    // 1. Lambda Function
    // Eine Lambda Function ist ein Programmartefakt wie ein Objekt. Sie kann referenziert werden.

    //Lambda Funktion
    // (Dish d) => {return d.getName();}

    //Lambda Expression
    //(Dish d) => d.getName()

    //Lambda Short
    //(Dish d) => Dish::getName()

    // 2. Verwendung von Lambda Funktion
    // überall wo ein Funktionales Interface als Typ verwendet wird kann eine Lambda Funktion verwendet werden.

    //3. Funktionales Interface:
    //  1) Annotaion @FunctionalInterface
    //  2) Nur eine Methodensignatur wird definiert

    //4. Systemdefined Functional Interfaces
    // a) Predicate     test(T -> boolean)
    // b) Consumer      accept(T -> void)
    // c) Function      apply(T -> R)
    // d) Runnable      run (() -> void)

    //5. Stream API
    // Die Stream API gibt dem Programmierer die Möglichkeit Collections von Objekten und deklarativ zu verarbeiten
    public static void main(String[] args)
    {
        Consumer<Apple>c;

        List<Dish> dishes = Arrays.asList(
                new Dish("Wiener Schnitzl", Dish.EDishType.MEAT, 15),
                new Dish("Bebo", Dish.EDishType.OTHER, 10),
                new Dish("Cordon Bleu", Dish.EDishType.MEAT, 17),
                new Dish("Karpfen", Dish.EDishType.FISH, 20)
        );

        List<Dish> excellentFood = new ArrayList<>();
        for (Dish d: dishes)
        {
            if(d.getType() == Dish.EDishType.MEAT)
            {
                excellentFood.add(d);
            }
        }

        List<String> dishNames = new ArrayList<>();

        for (Dish d:excellentFood)
        {
            dishNames.add(d.getName());
        }

        for (String s:dishNames)
        {
            System.out.println(s);
        }

        dishes.parallelStream()
                .filter((Dish d) -> {return d.getType() == Dish.EDishType.FISH;})
                .map(Dish::getName)
                .forEach(System.out::println);


      /*  Predicate<Apple> predicate = (Apple a) -> {return a.getTaste() == Apple.Taste.SWEET;};
        Predicate<Apple> predicate2 = (Apple a) -> a.getTaste() == Apple.Taste.SOUR;

        AppleDealer david = new AppleDealer();

        List<Apple> pickedApples= david.pick(Arrays.asList(
                new Apple("Pink Lady",Apple.Color.RED,3, Apple.Taste.SWEET),
                new Apple("Gouda",Apple.Color.YELLOW,7, Apple.Taste.SOUR),
                new Apple("Kronprinz Rudolf",Apple.Color.RED,10, Apple.Taste.SOUR),
                new Apple("Horneburger Pfannkuchenapfel",Apple.Color.RED,11, Apple.Taste.SWEET)
                ),
                predicate2);

        for (Apple a: pickedApples)
        {
            System.out.println(a.toString());
        }

        List<Dish> dishes = Arrays.asList(
                new Dish("Wiener Schnitzl", Dish.EDishType.MEAT, 15),
                new Dish("Bebo", Dish.EDishType.OTHER, 10),
                new Dish("Cordon Bleu", Dish.EDishType.MEAT, 17),
                new Dish("Karpfen", Dish.EDishType.FISH, 20)
        );

       List<Dish> pickedDishes = david.pick(dishes,(Dish d) -> {return d.getType() == Dish.EDishType.MEAT;});

        for (Dish d:pickedDishes
             ) {
            System.out.println(d.toString());

        }*/


    }
}
