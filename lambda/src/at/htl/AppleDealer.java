package at.htl;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class AppleDealer {
    public <T> List<T> pick(List<T> apples, Predicate<T> p) {
        List<T> pickedApples = new ArrayList<>();

        for (T a : apples)
        {
            if(p.test(a)) {
                pickedApples.add(a);
            }
        }

     return pickedApples;
    }
}
