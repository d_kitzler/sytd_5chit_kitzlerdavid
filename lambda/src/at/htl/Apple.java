package at.htl;

import java.io.Serializable;

public class Apple implements Serializable
{
    private String name;

    private Color color;

    private Integer weight;

    private Taste taste;

    public enum Color { GREEN, RED, YELLOW}

    public enum Taste { SWEET, SOUR }

    public Apple(String name, Color color, Integer weight, Taste taste) {
        this.name = name;
        this.color = color;
        this.weight = weight;
        this.taste = taste;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Taste getTaste() {
        return taste;
    }

    public void setTaste(Taste taste) {
        this.taste = taste;
    }

    @Override
    public String toString() {
        return "Apple{" +
                "name='" + name + '\'' +
                ", color=" + color +
                ", weight=" + weight +
                ", taste=" + taste +
                '}';
    }
}
