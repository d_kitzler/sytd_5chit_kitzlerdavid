package at.htl.restaurant.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Min;

@Entity
@Table(name = "zutaten")
public class Ingredient extends AEntity
{
    @Column(name = "BEZEICHNUNG", nullable = false, unique = true)
    private String description;

    @Min(0)
    @Column(name = "LAGERBESTAND")
    private Integer stock;

    @Column(name = "VERSION")
    private Long version;
}