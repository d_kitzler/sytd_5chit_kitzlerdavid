package at.htl.restaurant.domain;

import at.htl.restaurant.model.Ingredient;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IIngredientRepository  extends JpaRepository<Ingredient,Long>
{

}
