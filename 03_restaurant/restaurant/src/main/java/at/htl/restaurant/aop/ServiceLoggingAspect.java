package at.htl.restaurant.aop;

import org.aopalliance.intercept.Joinpoint;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

@Aspect
@Component
public class ServiceLoggingAspect
{
    private static Logger log = LoggerFactory.getLogger(ServiceLoggingAspect.class);

    @Before("execution(* at.htl.restaurant.service.BranchResource.*(..))")
    public void logCall(JoinPoint jp) {
        MethodSignature methodSignature = (MethodSignature) jp.getSignature();
        log.info(String.format("method call: %s", methodSignature.toString()));
    }
}
