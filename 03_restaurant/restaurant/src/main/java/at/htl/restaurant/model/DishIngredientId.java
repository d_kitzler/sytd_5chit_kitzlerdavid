package at.htl.restaurant.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
@AllArgsConstructor
@NoArgsConstructor
public class DishIngredientId implements Serializable
{
    @ManyToOne
    @JoinColumn(name = "gericht_id")
    private Dish dish;

    @ManyToOne
    @JoinColumn(name = "zutat_id")
    private Ingredient ingredient;
}
