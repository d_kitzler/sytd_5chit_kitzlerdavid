package at.htl.restaurant.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "GERICHTE")
public class Dish extends AEntity
{
    @NotNull
    @Column(name = "bezeichnung", nullable = false, unique = true)
    private String name;

    @Min(0)
    @Column(name = "PREIS", nullable = false)
    private Double price;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "art", length = 11, nullable = false)
    private EDishType dishType;

    @Column(name = "VERSION")
    private Long version;
}
