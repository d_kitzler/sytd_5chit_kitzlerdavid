package at.htl.restaurant.domain;

import at.htl.restaurant.model.Branch;
import at.htl.restaurant.model.Dish;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IBranchRepository extends JpaRepository<Branch,Long>
{
    @Query("SELECT count(t.id) FROM Tables t JOIN Branch b ON b = t.branch WHERE b = :branchInput")
    int getTableCountByBranch(@Param("branchInput") Branch branch);

    List<Branch> getBranchByNameContainingOrderByNameAsc(String token);

    @Query("SELECT y.employeeId.branch FROM CookDish x JOIN BrachEmployee y ON x.cookDishID.cook = y.employeeId.employee WHERE x.cookDishID.dish = :inputDish ORDER BY y.employeeId.branch.name")
    List<Branch> getBranchesByDish(@Param("inputDish") Dish dish);
}
