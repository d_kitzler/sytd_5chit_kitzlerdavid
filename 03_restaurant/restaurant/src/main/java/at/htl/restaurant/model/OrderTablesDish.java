package at.htl.restaurant.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Bestellungen_Tische_Gerichte")
public class OrderTablesDish implements Serializable
{
    @EmbeddedId
    private OrderTablesDishId orderTablesDishId ;


}
