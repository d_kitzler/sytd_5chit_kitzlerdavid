package at.htl.restaurant.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
@AllArgsConstructor
@NoArgsConstructor
@Data
public class BranchEmployeeId implements Serializable
{
    @ManyToOne
    @JoinColumn(name = "aemployee_id")
    private Employee employee;

    @ManyToOne
    @JoinColumn(name = "branch_list_id")
    private Branch branch;
}
