package at.htl.restaurant.model;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "TISCHE")
public class Tables extends AEntity
{
    @NotNull
    @Column(name = "ist_raucher_tisch", nullable = false)
    private Boolean isSmokerTable;

    @Column(name = "VERSION")
    private Long version;

    @Min(0)
    @NotNull
    @Column(name = "tisch_nr", nullable = false)
    private Integer tableNr;

    @ManyToOne
    @JoinColumn(name = "filiale_id", nullable = false)
    private Branch branch;
}
