package at.htl.restaurant.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class RunTimeAspect {
    private static Logger log = LoggerFactory.getLogger(ServiceLoggingAspect.class);

    @Around("execution(* at.htl.restaurant.service.*.create(*))")
    public Object execute(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {

        Object result = null;

        long begin = System.currentTimeMillis();

        try {
            result = proceedingJoinPoint.proceed();
        }

        catch (Exception e)
        {
            log.info("exception! something went wrong :(");
        }

        long end = System.currentTimeMillis();
        log.info("runtime is: "+ (end-begin) +"milliseconds");
        return result;
    }
}

