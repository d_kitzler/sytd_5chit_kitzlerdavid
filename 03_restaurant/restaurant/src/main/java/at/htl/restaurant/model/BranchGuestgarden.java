package at.htl.restaurant.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "filialen_gastgarten")
public class BranchGuestgarden extends Branch
{
    @Temporal(TemporalType.TIME)
    @NotNull
    @Column(name = "schluss_zeit", nullable = false)
    private Date endTime;

    @Temporal(TemporalType.TIME)
    @NotNull
    @Column(name = "oeffnungs_zeit",nullable = false)
    private Date beginTime;
}
