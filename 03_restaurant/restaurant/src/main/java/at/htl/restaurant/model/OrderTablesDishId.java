package at.htl.restaurant.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Embeddable
@AllArgsConstructor
@NoArgsConstructor
public class OrderTablesDishId implements Serializable
{
    @ManyToOne
    @JoinColumn(name = "tisch_id")
    private Tables table;

    @ManyToOne
    @JoinColumn(name = "gericht_id")
    private Dish dish;

    @ManyToOne
    @JoinColumn(name = "bestellungs_id")
    private Orders order;

    @Column(name = "version")
    private Integer version;
}
