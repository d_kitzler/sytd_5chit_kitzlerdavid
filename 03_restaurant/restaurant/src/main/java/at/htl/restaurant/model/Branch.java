package at.htl.restaurant.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "json-type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Branches.class, name = "branches"),
        @JsonSubTypes.Type(value = BranchGuestgarden.class, name = "branchGuestgarden")})

@Data
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "A_FILIALEN")
@Entity
public class Branch extends AEntity
{
    @Column(name = "VERSION")
    private Long version;

    @Column(name = "NAME", length = 50, nullable = false)
    private String name;

    @Column(name = "ADRESSE", length = 200, nullable = false)
    private String adresse;

    @Column(name = "BEZIRK", length = 200, nullable = false)
    private String bezirk;
}
