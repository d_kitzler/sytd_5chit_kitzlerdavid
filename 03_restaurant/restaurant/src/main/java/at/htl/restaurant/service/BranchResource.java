package at.htl.restaurant.service;

import at.htl.restaurant.domain.IBranchRepository;
import at.htl.restaurant.model.Branch;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;

@RequestMapping(path="/branches") //bezieht sicha auf den Endpunkt
@RestController //macht einen  Rest Service daraus
//Rest kommunziert auf HTTP Protokol, Anfrage und bekommt Antowrt zurück;
//Header: Metainformationen, welche Daten unterstützt
//Body: Info, die ich weitervermitteln möchte
//HTTP unterstützt Methoden
public class BranchResource
{
    private static Logger log = LoggerFactory.getLogger(BranchResource.class);

    @GetMapping(path ="/greet", produces = MediaType.TEXT_PLAIN_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public String greeting()
    {
        log.info("greeting was issued");

        return "Hello Mr. Clit";
    }

    @Autowired
    private IBranchRepository branchRepository;

    @Transactional
    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE) //welches Datenformat kann von dem Service erzeugt werden
    @ResponseStatus(HttpStatus.OK)
    public Branch read(@PathVariable("id") Long id)
    {
        Branch branch = branchRepository.getOne(id);
        log.info("loaded branch with id: " + branch.toString() );

        return (Branch) Hibernate.unproxy(branch);
    }

    @Transactional
    @PostMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public Branch create(@RequestBody Branch branch) //Request Body stehen die notwendigen Daten zum Befüllen
    {
        branch = branchRepository.save(branch);
        log.info("create Branch: "+branch.getId());

        return (Branch) Hibernate.unproxy(branch);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping(path="/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Transactional
    public void update(@PathVariable("id") Long id, @RequestBody Branch branch)
    {
        branch = branchRepository.save(branch);

        log.info("updated branch with id: "+ id);
    }

    @Transactional
    @DeleteMapping(path = "/delete/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") Long id)
    {
        branchRepository.deleteById(id);

        log.info("branch removed id: "+ id);
    }
}
