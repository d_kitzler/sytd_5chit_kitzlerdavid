package at.htl.restaurant.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name="FILIALEN")
public class Branches extends Branch
{

}
