package at.htl.restaurant.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "SESSEL")
public class Chair extends AEntity
{
    @Size(max = 10)
    @NotNull
    @Column(name = "code", nullable = false, unique = true, length = 10)
    private String code;

    @ManyToOne
    @JoinColumn (name = "table_id")
    private Tables tables;

    @Column(name = "version")
    private Long version;
}
