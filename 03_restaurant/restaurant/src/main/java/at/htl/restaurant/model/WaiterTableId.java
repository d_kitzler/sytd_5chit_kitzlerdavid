package at.htl.restaurant.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
@AllArgsConstructor
@NoArgsConstructor
public class WaiterTableId implements Serializable
{
    @ManyToOne
    @JoinColumn(name = "WAITER_ID")
    private Waiter waiter;

    @ManyToOne
    @JoinColumn(name = "TABLE_LIST_ID")
    private Tables table;
}
