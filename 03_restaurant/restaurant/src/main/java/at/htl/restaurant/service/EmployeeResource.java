package at.htl.restaurant.service;

import at.htl.restaurant.domain.IBranchRepository;
import at.htl.restaurant.domain.IEmployeeRepository;
import at.htl.restaurant.model.Branch;
import at.htl.restaurant.model.Employee;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;

//@RequestMapping(path="/employees")
//@RestController
/*public class EmployeeResource
{
    private static Logger log = LoggerFactory.getLogger(EmployeeResource.class);

    @Autowired
    private IEmployeeRepository employeeRepository;

    @Transactional
    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public Employee read(@PathVariable("id") Long id)
    {
        Employee employee = employeeRepository.getOne(id);
        log.info("loaded employee with id: " + employee.toString() );

        return (Employee) Hibernate.unproxy(employee);
    }

    @Transactional
    @PostMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public Employee create(@RequestBody Employee employee)
    {
        employee = employeeRepository.save(employee);
        log.info("create Branch: "+employee.getId());

        return (Employee) Hibernate.unproxy(employee);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping(path="/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @Transactional
    public void update(@PathVariable("id") Long id, @RequestBody Employee employee)
    {
        employee = employeeRepository.save(employee);

        log.info("updated employee with id: "+ id);
    }

    @Transactional
    @DeleteMapping(path = "/delete/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") Long id)
    {
        employeeRepository.deleteById(id);

        log.info("employee removed id: "+ id);
    }
    }
    */

