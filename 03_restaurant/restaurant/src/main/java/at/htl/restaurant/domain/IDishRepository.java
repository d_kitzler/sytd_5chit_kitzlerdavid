package at.htl.restaurant.domain;

import at.htl.restaurant.model.Branch;
import at.htl.restaurant.model.Dish;
import at.htl.restaurant.model.Ingredient;
import at.htl.restaurant.model.Tables;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IDishRepository extends JpaRepository<Dish, Long>
{
    @Query("SELECT x.dishIngredientId.dish FROM DishIngredient x WHERE x.dishIngredientId.ingredient = :ingredientInput")
    List<Dish> getDishesByIngredient(@Param("ingredientInput")Ingredient ingredient);

    @Query("SELECT count(x.orderTablesDishId.order) FROM OrderTablesDish x where x.orderTablesDishId.dish = :dishInput")
    int getCountOrders(@Param("dishInput")Dish dish);


    @Query("SELECT x.orderTablesDishId.dish FROM OrderTablesDish x GROUP BY x.orderTablesDishId.dish.id " +
    "HAVING count(x.orderTablesDishId.dish.id) >= ALL " +
    "(SELECT COUNT(x.orderTablesDishId.dish.id) FROM OrderTablesDish y " +
    "GROUP BY y.orderTablesDishId.dish.id)")
     List<Dish> getMaxOrOrderByDish();

   // @Query()
   // List<Tables> getMaxSalesTableByBranch(@Param("branchInput") Branch branch);
}

