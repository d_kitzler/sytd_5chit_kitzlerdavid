package at.htl.restaurant.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "json-type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Waiter.class, name = "waiter"),
        @JsonSubTypes.Type(value = Cook.class, name = "cook")})

@Data
@Inheritance(strategy = InheritanceType.JOINED)
@Entity
@Table(name = "ANGESTELLTE")
public class Employee extends AEntity
{
    @Size(max = 50)
    @NotNull
    @Column(name = "vorname", length = 50, nullable = false)
    private String firstname;

    @Size(max = 50)
    @NotNull
    @Column(name = "nachname", length = 50, nullable = false)
    private String lastname;

    @Size(max = 10)
    @NotNull
    @Column(name = "sozialversicherungs_nr", nullable = false, unique = true, length = 10)
    private String socialSecurityNumber;

    @Column(name = "version")
    private Long version;
}
