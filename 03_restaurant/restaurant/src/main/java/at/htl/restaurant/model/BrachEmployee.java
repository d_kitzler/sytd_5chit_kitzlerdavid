package at.htl.restaurant.model;

import lombok.Data;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "FILIALEN_ANGESTELLTE")
@Data
public class BrachEmployee implements Serializable
{
    @EmbeddedId
    private BranchEmployeeId employeeId;

}
