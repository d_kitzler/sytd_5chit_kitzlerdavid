package at.htl.restaurant.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.*;
import java.io.Serializable;

@Embeddable
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CookDishId implements Serializable
{
    @ManyToOne
    @JoinColumn(name = "COOK_ID")
    private Cook cook;

    @ManyToOne
    @JoinColumn(name = "DISH_LIST_ID")
    private Dish dish;
}
