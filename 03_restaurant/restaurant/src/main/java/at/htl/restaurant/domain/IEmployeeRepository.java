package at.htl.restaurant.domain;

import at.htl.restaurant.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IEmployeeRepository extends JpaRepository<Employee, Long>
{
    public Employee readEmployeeById(Long id);
    public Employee readEmployeeBySocialSecurityNumber(String socialSecurityNumber);
    public List<Employee> readEmployeeByLastnameOrderByLastname(String lastname);
    public List<Employee> readEmployeeByLastnameContains(String token);

    @Query("select c from Employee c where type(c)=Cook order by c.lastname asc")
    List<Employee> getCooks();

    @Query("SELECT x.employeeId.employee FROM BrachEmployee x join x.employeeId.employee e WHERE x.employeeId.branch.id = :branchId AND type(e) = Waiter")
    default List<Waiter> getWaitersByBranchID(@Param("branchId") Long id) {
        return null;
    }

    //@Query("select x.waiterTableId.taable FROM WaiterTable WHERE x.waiterTableId.waiter =: waiter")
   // List<Tables> getTablesByWaiter(@Param("waiter") Waiter waiter);

    @Query("SELECT x.cookDishID.cook FROM CookDish x WHERE x.cookDishID.dish = :dishInput")
    List<Cook> getCooksByDish(@Param("dishInput") Dish dish);
}
