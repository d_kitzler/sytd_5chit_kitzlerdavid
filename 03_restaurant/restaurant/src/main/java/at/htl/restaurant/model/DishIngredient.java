package at.htl.restaurant.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "GERICHTE_ZUTATEN")
public class DishIngredient implements Serializable
{
    @EmbeddedId
    private DishIngredientId dishIngredientId;

    @NotNull
    @Min(0)
    @Column(name = "MENGE", nullable = false)
    private Integer amount;

    @NotNull
    @Column(name = "version", nullable = false)
    private Integer version;



}
