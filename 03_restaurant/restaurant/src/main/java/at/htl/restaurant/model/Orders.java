package at.htl.restaurant.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.sql.Time;
import java.time.LocalDateTime;

@Entity
@Table(name = "BESTELLUNGEN")
public class Orders extends AEntity
{
    @NotNull
    @Column(name = "bestellungs_zeitpunkt", nullable = false)
    private Time orderTime;

    @NotNull
    @Column(name = "bestellungs_id", unique = true, nullable = false)
    private Integer orderId;

    @NotNull
    @Column(name = "verlauf", nullable = false)
    private String progress;

    @NotNull
    @Column(name = "version", nullable = false)
    private Integer amount;
}