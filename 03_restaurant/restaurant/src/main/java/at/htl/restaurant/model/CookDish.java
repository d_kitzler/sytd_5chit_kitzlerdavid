package at.htl.restaurant.model;

import lombok.Data;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "KOECHE_GERICHTE")
@Data
public class CookDish implements Serializable {

    @EmbeddedId
    private CookDishId cookDishID;

}
