package at.htl.restaurant.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "KELLNER_TISCHE")
public class WaiterTable implements Serializable
{
    @EmbeddedId
    private WaiterTableId waiterTableId;
}
