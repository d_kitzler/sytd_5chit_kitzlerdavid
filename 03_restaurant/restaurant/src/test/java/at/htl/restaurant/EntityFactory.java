package at.htl.restaurant;

import at.htl.restaurant.model.*;

public class EntityFactory
{
    public static Branch createDefaultBranchData()
    {
        Branches branch = new Branches();

        branch.setName("Pizza AlCapone");
        branch.setAdresse("Kirchberg am Wagram, Markplatz 3");
        branch.setBezirk("3470");
        branch.setVersion(new Long(1L));

        return branch;
    }

    public static Employee createDefaultEmployeeData()
    {
        Waiter waiter = new Waiter();
        waiter.setFirstname("David");
        waiter.setLastname("Kitzler");
        waiter.setSocialSecurityNumber("1337200100");
        waiter.setVersion(new Long(1L));

        return waiter;
    }
}
