package at.htl.restaurant.integration;

import at.htl.restaurant.EntityFactory;
import at.htl.restaurant.domain.IBranchRepository;
import at.htl.restaurant.model.Branch;
import at.htl.restaurant.model.Branches;
import at.htl.restaurant.service.BranchResource;
import org.hibernate.Hibernate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
public class BranchResourceIntegrationTest
{
    private static Logger log = LoggerFactory.getLogger(BranchResourceIntegrationTest.class);

    @Autowired
    private IBranchRepository branchRepository;

    @Transactional
    @Test
    public void greetingClit()
    {
        log.info("hallo Clit");
        String requestURL = String.format("%s/%s/%s", "http://127.0.0.1:8181", "restaurant", "branches/greet");

        RestTemplate restClient= new RestTemplate();

        restClient.getForEntity(requestURL, String.class);
        ResponseEntity<String> httpResponse = restClient.getForEntity(requestURL, String.class);
        assertEquals(httpResponse.getStatusCode(), HttpStatus.OK);

        String msg = httpResponse.getBody();

        assertEquals("Hello Mr. Clit", msg);
    }

    @Transactional
    @Test
    public void readTest()
    {
        log.info("hallo Clit");
        String requestURL = String.format("%s/%s/%s", "http://127.0.0.1:8181", "restaurant", "branches/1");

        RestTemplate restClient= new RestTemplate();

        restClient.getForEntity(requestURL, String.class);
        ResponseEntity<Branch> httpResponse = restClient.getForEntity(requestURL, Branch.class);

        Branch branch = httpResponse.getBody();

        assertEquals(branchRepository.getOne(1L).getId(),branch.getId());
    }

    @Transactional
    @Test
    public void createBranch()
    {
        String requestURL = String.format("%s/%s/%s", "http://127.0.0.1:8181", "restaurant", "branches");

        Branch branch = EntityFactory.createDefaultBranchData();
        HttpEntity<Branch> httpData = new HttpEntity<>(branch);

        RestTemplate restClient = new RestTemplate();
        ResponseEntity<Branch> httpResponse = restClient.postForEntity(requestURL, httpData, Branch.class);

        assertEquals(HttpStatus.CREATED, httpResponse.getStatusCode());

        Branch createdBranch = httpResponse.getBody();

        assertNotNull(createdBranch.getId());
    }

    @Transactional
    @Test
    public void updateBranch()
    {
        String requestURL = String.format("%s/%s/%s", "http://127.0.0.1:8181", "restaurant", "branches/1");

        Branch branch = (Branch) Hibernate.unproxy(branchRepository.getOne(1L));
        branch.setBezirk("3470");

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON_UTF8));

        HttpEntity<Branch> requestData = new HttpEntity<>(branch, httpHeaders);
        RestTemplate restClient = new RestTemplate();

        restClient.exchange(requestURL, HttpMethod.PUT, requestData, Void.class);
        Branch updatedBranch = branchRepository.getOne(1L);

        assertEquals("3470", updatedBranch.getBezirk());
    }

    @Test
    public void deleteBranch()
    {
        Branch branch = EntityFactory.createDefaultBranchData();
        branch = branchRepository.save(branch);
        String requestURL = String.format("%s/%s/%s/%d", "http://127.0.0.1:8181", "restaurant", "branches/delete",branch.getId());

        log.info(requestURL);

        RestTemplate restClient = new RestTemplate();
        restClient.delete(requestURL);

        Optional<Branch> delBranch = branchRepository.findById(branch.getId());
        assertFalse(delBranch.isPresent());
    }
}
