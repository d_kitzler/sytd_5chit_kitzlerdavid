package at.htl.restaurant.integration;

import at.htl.restaurant.EntityFactory;
import at.htl.restaurant.domain.IBranchRepository;
import at.htl.restaurant.domain.IEmployeeRepository;
import at.htl.restaurant.model.Branch;
import at.htl.restaurant.model.Employee;
import org.hibernate.Hibernate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;


import javax.transaction.Transactional;

import java.util.Arrays;
import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
public class EmployeeResourceIntegrationTest
{
    private static Logger log = LoggerFactory.getLogger(EmployeeResourceIntegrationTest.class);

    @Autowired
    private IEmployeeRepository employeeRepository;

    @Test
    @Transactional
    public void readTest()
    {
        String requestURL = String.format("%s/%s/%s", "http://127.0.0.1:8181", "restaurant", "employees/1");

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Employee> httpResponse = restTemplate.getForEntity(requestURL, Employee.class);

        Employee employee = httpResponse.getBody();

        assertEquals(employeeRepository.getOne(1L).getId(),employee.getId());
    }

    @Transactional
    @Test
    public void createEmployee()
    {
        String requestURL = String.format("%s/%s/%s", "http://127.0.0.1:8181", "restaurant", "employees");

        Employee employee = EntityFactory.createDefaultEmployeeData();
        HttpEntity<Employee> httpData = new HttpEntity<>(employee);

        RestTemplate restClient = new RestTemplate();
        ResponseEntity<Employee> httpResponse = restClient.postForEntity(requestURL, httpData, Employee.class);

        assertEquals(HttpStatus.CREATED, httpResponse.getStatusCode());

        Employee createdEmployee = httpResponse.getBody();

        assertNotNull(createdEmployee.getId());
    }

    @Transactional
    @Test
    public void updateEmployee()
    {
        String requestURL = String.format("%s/%s/%s", "http://127.0.0.1:8181", "restaurant", "employees/1");

        Employee employee = (Employee) Hibernate.unproxy(employeeRepository.getOne(1L));
        employee.setFirstname("David");

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON_UTF8));

        HttpEntity<Employee> requestData = new HttpEntity<>(employee, httpHeaders);
        RestTemplate restClient = new RestTemplate();

        restClient.exchange(requestURL, HttpMethod.PUT, requestData, Void.class);
        Employee updatedEmployee = employeeRepository.getOne(1L);

        assertEquals("David", updatedEmployee.getFirstname());
    }

    @Test
    public void deleteEmployee()
    {
        Employee employee = EntityFactory.createDefaultEmployeeData();
        employee = employeeRepository.save(employee);
        String requestURL = String.format("%s/%s/%s/%d", "http://127.0.0.1:8181", "restaurant", "employees/delete",employee.getId());

        log.info(requestURL);

        RestTemplate restClient = new RestTemplate();
        restClient.delete(requestURL);

        Optional<Employee> delEmployee = employeeRepository.findById(employee.getId());
        assertFalse(delEmployee.isPresent());
    }
}
