package at.htl.restaurant.unit;

import at.htl.restaurant.domain.EmployeeQualifier;
import at.htl.restaurant.model.Cook;
import at.htl.restaurant.model.Employee;
import at.htl.restaurant.model.Waiter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class EmployeeFactory
{
    @Autowired
    private IDGenerator generator;

        @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
         @Bean("cook") @EmployeeQualifier("cook")
         public Employee createDefaultCook() {
         Cook cook = new Cook();
         cook.setLastname("Nagelmaier");
         cook.setFirstname("Jonas");
         cook.setSocialSecurityNumber(
                 generator.generateSozialSecurityCode());

        return cook;
    }

    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    @Bean("waiter") @EmployeeQualifier("waiter")
    public Employee createDefaultWaiter() {
        Waiter waiter = new Waiter();

        waiter.setLastname("Schandl");
        waiter.setFirstname("Lukas");
        waiter.setSocialSecurityNumber(
                generator.generateSozialSecurityCode());

        return waiter;
    }
}
