package at.htl.restaurant.unit;

import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

@Component
public class IDGenerator
{
    private AtomicLong seq = new AtomicLong(0);

    public String generateSozialSecurityCode() {
    Random random = new Random();
    int minDay = (int) LocalDate.of(1900, 1,
            1).toEpochDay();
    int maxDay = (int) LocalDate.of(2015, 1,
            1).toEpochDay();

     long randomDay = minDay +
            random.nextInt(maxDay - minDay);

    LocalDate randomBirthDate =
            LocalDate.ofEpochDay(randomDay);

   DateTimeFormatter formatter =
             DateTimeFormatter.ofPattern("ddMMyyyy");

    StringBuilder str = new StringBuilder();

     str.append(
            randomBirthDate.format(formatter));
     str.append(

             seq.incrementAndGet() + 10);

     return str.toString().substring(0, 10);
}
}
