package at.htl.restaurant.unit;

import at.htl.restaurant.domain.IBranchRepository;
import at.htl.restaurant.domain.IDishRepository;
import at.htl.restaurant.model.Branch;
import at.htl.restaurant.model.Dish;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

import java.util.List;

import static org.junit.Assert.*;


@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class BranchRepositoryUnitTest
{
    private static Logger LOG = LoggerFactory.getLogger(BranchRepositoryUnitTest.class);

    @Autowired //Lebenszyklus des Objekts wird vom System verwaltet; kann man auch bei Klassen machen
    public IBranchRepository branchRepository;

    @Autowired
    public IDishRepository dishRepository;

    @Transactional
    @Test
    public void getTableCountForBranch()
    {
        Branch branch = branchRepository.getOne(1L);
        int tableCount = branchRepository.getTableCountByBranch(branch);

        assertEquals(4,tableCount);
    }

    @Transactional
    @Test
    public void getABranchByNameContainingOrderByNameAsc()
    {
        List<Branch> branches = branchRepository.getBranchByNameContainingOrderByNameAsc("L");
        assertEquals(branches.size(), 3);
    }
    @Transactional
    @Test
    public void getBranchesByDish(){
        Dish dish = dishRepository.getOne(1l);
        List<Branch> branches = branchRepository.getBranchesByDish(dish);

        assertEquals(1,branches.size());
    }









}
