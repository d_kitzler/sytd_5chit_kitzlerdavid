package at.htl.restaurant.unit;

import at.htl.restaurant.domain.IDishRepository;
import at.htl.restaurant.domain.IEmployeeRepository;
import at.htl.restaurant.model.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class EmployeeRepositoryUnitTest {
    private static Logger LOG = LoggerFactory.getLogger(BranchRepositoryUnitTest.class); //wenn ich eine Info an die Außenwelt weitergeben will kann ich es in Console
    // oder Datei schreiben; automatisch in Konsole

    @Test
    public void testHelloWord() {
        LOG.info("Hallo Welt");
    }

    @Autowired //Automatisch instanz zugewissen durch Dependency Injection
    private IEmployeeRepository employeeRepository;

    @Autowired
    private IDishRepository dishRepository;

    @Transactional
    @Test
    public void readEmployeeById() {
        Employee employee = employeeRepository.getOne(1l);
        assertEquals(new Long(1L), employee.getId());
    }

    @Transactional
    @Test
    public void readEmployeeBySocialSecurityNumber() {
        Employee employee = employeeRepository.readEmployeeBySocialSecurityNumber("17091999");
        assertEquals("Nikolaus", employee.getFirstname().trim());

        LOG.info(employee.getFirstname());
    }

    @Transactional
    @Test
    public void readEmployeeByLastnameOrderByLastname()
    {
        List<Employee> employees = employeeRepository.readEmployeeByLastnameOrderByLastname("Leister");
        assertEquals(1, employees.size());
    }

    @Transactional
    @Test
    public void readEmployeeByLastnameContains()
    {
        List<Employee> employees = employeeRepository.readEmployeeByLastnameContains("K");
        assertEquals(employees.size(), 3);
    }

    @Transactional
    @Test
    public void readAllCocks()
    {
        List<Employee> cooks = employeeRepository.getCooks();
        assertEquals(cooks.size(), 7);
    }

    @Transactional
    @Test
    public void readWaitersByBranch()
    {
        List<Waiter> waiters = employeeRepository.getWaitersByBranchID(1l);
        assertEquals(3, waiters.size());
    }

    @Transactional
    @Test
    public void readCooksByDish(){
        Dish dish = dishRepository.getOne(1l);
        List<Cook> cooks = employeeRepository.getCooksByDish(dish);
        assertEquals(2,cooks.size());
    }

    /*@Transactional
    @Test
    public void getTablesbyWaiter()
    {
        Waiter waiter = employeeRepository.getWaitersByBranchID(1l).get(0);
        List<Tables> tables = employeeRepository.getTablesByWaiter(waiter);

        assertEquals(tables.size(), 4);
    }

     */
}
