package at.htl;

import java.util.concurrent.ArrayBlockingQueue;

public class MessageQueue extends ArrayBlockingQueue<Message>
{
    private static MessageQueue instance = new MessageQueue();

    private MessageQueue()
    {
        super(30);
    }

    public static MessageQueue getInstance()
    {
        return instance;
    }
}
