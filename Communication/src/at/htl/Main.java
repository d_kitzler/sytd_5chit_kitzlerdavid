package at.htl;

import java.util.concurrent.CountDownLatch;

public class Main {

    public static void main(String[] args)
    {
	    Thread t = new Thread(new Producer());
	    t.start();

        int consumerCount = 20;

        CountDownLatch latch = new CountDownLatch(consumerCount);

        for (int i = 0; i < consumerCount ; i++)
        {

            Thread tConsumer = new Thread(new Consumer(latch, i+1));
            tConsumer.start();
            latch.countDown();
        }
    }
}
