package at.htl;

import java.util.concurrent.CountDownLatch;

public class Consumer implements Runnable
{
    private CountDownLatch countDownLatch;

    private int index;

    public Consumer(CountDownLatch countDownLatch, int index)
    {
        this.countDownLatch = countDownLatch;
        this.index = index;
    }

    @Override
    public void run() {
        try {
            countDownLatch.await();

            while (true)
            {
                Message messsage = MessageQueue.getInstance().take();
                System.out.println("consumer"+index+" :" + messsage.getContent());

                Thread.sleep(50);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
