package at.htl;

import java.util.List;

public class SecondEffectCalculator implements IAnalyzer
{
    IPumpkinBehaviour pumpkinBehaviour;

    @Override
    public IPumpkinBehaviour Test(List<LightRainfallRecord> inputList, Integer index)
    {
        for(index = 0; index < inputList.size(); index++)
        {
            if (index >= 4) {
                int water = 0;

                for (int j = 0; j < 5; j++) {
                    water += 100 - inputList.get(index - j).rainFallAmount();
                }

                if (water < 10)
                {
                    return pumpkinBehaviour = new SecondEffectDecorator(pumpkinBehaviour);
                }
            }
        }
        return null;
    }
}
