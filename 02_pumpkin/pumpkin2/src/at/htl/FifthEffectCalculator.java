package at.htl;

import java.util.List;

public class FifthEffectCalculator implements IAnalyzer {
    IPumpkinBehaviour pumpkinBehaviour;

    @Override
    public IPumpkinBehaviour Test(List<LightRainfallRecord> inputList, Integer i) {

        for (i = 0; i < inputList.size(); i++) {
            if (i >= 1) {
                int water = 0;

                for (int j = 0; j < 2; j++) {
                    water += inputList.get(i - j).rainFallAmount();
                }

                if (i < 30)
                    return pumpkinBehaviour = new FifthEffectDecorator();
            }
        }
        return null;
    }
}