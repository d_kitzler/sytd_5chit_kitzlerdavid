package at.htl;

import java.util.List;

public class FirstEffectCalculator implements IAnalyzer
{
    IPumpkinBehaviour pumpkinBehaviour;

    @Override
    public IPumpkinBehaviour Test(List<LightRainfallRecord> inputList, Integer index)
    {
        return pumpkinBehaviour = new FirtstEffectDecorator(pumpkinBehaviour);
    }
}
