package at.htl;

import java.util.List;

public class ThirdEffectCalculator implements IAnalyzer
{
    IPumpkinBehaviour pumpkinBehaviour;

    @Override
    public IPumpkinBehaviour Test(List<LightRainfallRecord> inputList, Integer index) {

        for(index = 0; index < inputList.size(); index++)
        {
            if (index >= 9) {
                int water = 0;

                for (int j = 0; j < 10; j++) {
                    water += 100 - inputList.get(index - j).rainFallAmount();
                }

                if (index < 10)
                    return pumpkinBehaviour = new ThirdEffectDecorator();
            }
        }
        return null;
    }
}
