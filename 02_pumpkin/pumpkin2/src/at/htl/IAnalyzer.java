package at.htl;

import java.util.List;

public interface IAnalyzer
{
    public IPumpkinBehaviour Test(List<LightRainfallRecord> inputList, Integer index);
}
