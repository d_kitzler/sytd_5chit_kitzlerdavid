package at.htl;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class LightRainfallRecord
{
    private Integer lightAmount;

    public Integer getLightAmount ()
    {
        return lightAmount;
    }

    public Integer rainFallAmount()
    {
        return 100 - lightAmount;
    }
}
