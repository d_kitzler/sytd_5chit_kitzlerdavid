package at.htl;

import lombok.Data;

@Data
public class EffectRecord
{
   private Integer quota;
   private EEffectType effect;
}
