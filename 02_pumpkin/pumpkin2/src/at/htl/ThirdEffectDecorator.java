package at.htl;

public class ThirdEffectDecorator implements IPumpkinBehaviour
{
    IPumpkinBehaviour pumpkinBehaviour;

    @Override
    public int getGrowth(int light, int water)
    {
        return pumpkinBehaviour.getGrowth(light,water) * 0;
    }

    @Override
    public String getDescripiton() {
        return pumpkinBehaviour.getDescripiton() + "E3, ";
    }
}
