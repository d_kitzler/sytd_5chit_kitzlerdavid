package at.htl;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class SecondEffectDecorator implements IPumpkinBehaviour
{
    IPumpkinBehaviour pumpkinBehaviour;

    @Override
    public int getGrowth(int light, int water) {
        return pumpkinBehaviour.getGrowth(light,water) / 2;
    }

    @Override
    public String getDescripiton() {
        return pumpkinBehaviour.getDescripiton() + "E2 + ";
    }
}
