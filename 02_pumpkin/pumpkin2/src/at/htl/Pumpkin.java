package at.htl;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Pumpkin implements IPumpkinBehaviour
{
    int baseSize;

    @Override
    public int getGrowth(int light, int water)
    {
        return baseSize;
    }

    @Override
    public String getDescripiton() {
        return "Pumpkin: ";
    }
}
