package at.htl;

public class FifthEffectDecorator implements IPumpkinBehaviour
{
    IPumpkinBehaviour pumpkinBehaviour;

    @Override
    public int getGrowth(int light, int water) {
        return pumpkinBehaviour.getGrowth(light,water) - 1;
    }

    @Override
    public String getDescripiton() {
        return pumpkinBehaviour.getDescripiton() + "E5 +";
    }
}
