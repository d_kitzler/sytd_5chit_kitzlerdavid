package at.htl;

public interface IPumpkinBehaviour
{
    public int getGrowth(int light, int water);
    public String getDescripiton();
}
