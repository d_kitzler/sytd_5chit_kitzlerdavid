package at.htl;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class SimulationResultRecord
{
    @NonNull
   private Integer growthAmount;

    @Getter
   private List<EffectRecord> effectRecords = new ArrayList<EffectRecord>();
}
