package at.htl;

import java.util.List;

public class FourthEffectCalculator implements IAnalyzer
{
    IPumpkinBehaviour pumpkinBehaviour;

    @Override
    public IPumpkinBehaviour Test(List<LightRainfallRecord> inputList, Integer index)
    {
        if (inputList.get(index).rainFallAmount() >= 50)
            return pumpkinBehaviour = new FourthEffectDecorator(pumpkinBehaviour);

        return null;
    }
}
