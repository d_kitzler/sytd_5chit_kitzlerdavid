package at.htl;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class FirtstEffectDecorator implements IPumpkinBehaviour
{
    IPumpkinBehaviour pumpkinBehaviour;

    @Override
    public int getGrowth(int light, int water)
    {
        int growthPercentage = light / 20;
        return pumpkinBehaviour.getGrowth(light, water) + growthPercentage;
    }

    @Override
    public String getDescripiton() {
        return pumpkinBehaviour.getDescripiton() + "E1 + ";
    }
}
