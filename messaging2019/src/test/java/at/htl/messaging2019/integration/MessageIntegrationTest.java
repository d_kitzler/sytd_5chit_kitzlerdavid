package at.htl.messaging2019.integration;

import at.htl.messaging2019.model.Employee;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
public class MessageIntegrationTest
{
    @Test
    public void sendingTest()
    {
        String requestURL = String.format("%s/%s/%s", "http://127.0.0.1:8181", "webshop", "messaging");

        Employee employee = new Employee("David", "Kitzler");

        RestTemplate restclient = new RestTemplate();
        HttpEntity<Employee> httpData = new HttpEntity<>(employee);

        ResponseEntity<Employee> httpResponse = restclient.postForEntity(requestURL, httpData, Employee.class);

        assertEquals(HttpStatus.OK, httpResponse.getStatusCode());
    }
}
