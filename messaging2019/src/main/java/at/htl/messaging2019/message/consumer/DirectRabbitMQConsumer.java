package at.htl.messaging2019.message.consumer;

import at.htl.messaging2019.model.Employee;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DirectRabbitMQConsumer
{
    @Autowired
    private ObjectMapper objectMapper;

    @RabbitListener(queues = "htl.pc.queue1")
    public void listen(String message) throws JsonProcessingException {
        Employee employee = objectMapper.readValue(message, Employee.class);
    }
}
