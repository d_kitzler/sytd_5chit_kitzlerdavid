package at.htl.messaging2019.message.producer;

import at.htl.messaging2019.model.Employee;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DirectRabbitMQProducer
{
    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void sendDirectMessage(Employee employee)
            throws JsonProcessingException {
        String message = objectMapper.writeValueAsString(employee);
        rabbitTemplate.convertAndSend("htl.pc.exchange","q1", message);
    }
}
