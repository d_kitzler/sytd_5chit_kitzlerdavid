package at.htl.messaging2019;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Messaging2019Application {

    public static void main(String[] args) {
        SpringApplication.run(Messaging2019Application.class, args);
    }

}
