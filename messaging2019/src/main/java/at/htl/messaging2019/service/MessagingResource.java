package at.htl.messaging2019.service;

import at.htl.messaging2019.message.producer.DirectRabbitMQProducer;
import at.htl.messaging2019.model.Employee;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.print.attribute.standard.Media;
import javax.validation.Valid;

@RequestMapping(path="/messaging")
@RestController
public class MessagingResource
{
    private static final Logger log = LoggerFactory.getLogger(MessagingResource.class);

    @Autowired
    private DirectRabbitMQProducer directProducer;

    @ResponseStatus(HttpStatus.OK)
    @PostMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void sendMessage(@RequestBody @Valid Employee employee) throws JsonProcessingException {
        directProducer.sendDirectMessage(employee);
    }
}
