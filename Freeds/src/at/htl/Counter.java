package at.htl;

import java.util.concurrent.Semaphore;

public class Counter
{
    private int value = 0;

    private Semaphore sem;

    public Counter(Semaphore sem)
    {
        this.sem = sem;
    }

    public int IncrementAndGet()
    {
        try {
            sem.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        value +=1;
        try {
            Thread.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        sem.release();

        return value;
    }
}
