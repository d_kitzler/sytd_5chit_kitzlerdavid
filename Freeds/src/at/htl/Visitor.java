package at.htl;

import java.util.concurrent.CountDownLatch;

public class Visitor implements Runnable
{
    private CountDownLatch latch;

    private Counter counter;

    public Visitor(CountDownLatch latch, Counter counter)
    {
        this.latch = latch;
        this.counter = counter;
    }

    @Override
    public void run()
    {
        try {
            latch.await();
            int value = counter.IncrementAndGet();

            System.out.println(value);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
