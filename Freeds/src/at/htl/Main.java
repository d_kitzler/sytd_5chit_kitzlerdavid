package at.htl;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;

public class Main {

    public static void main(String[] args)
    {
        Semaphore semaphore = new Semaphore(1);
        Counter counter = new Counter(semaphore);
        int threadCount = 100;
        CountDownLatch latch = new CountDownLatch(threadCount);

        for (int i = 0; i < threadCount; i++)
        {
            Thread t = new Thread(new Visitor(latch,counter));
            t.start();
            latch.countDown();
        }
    }
}
