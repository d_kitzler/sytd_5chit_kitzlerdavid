package at.htl;

import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@NoArgsConstructor
@RequiredArgsConstructor
public class MilkDecorator implements IIngredient
{
    @NonNull
    private IIngredient ingredient;

    private String description = "Milch";
    private  Integer prize = 2;

    public String getDescription() {
        return String.format("%s %s", ingredient.getDescription(), description);
    }

    public Integer getPrize()
    {
        return ingredient.getPrize() + prize;
    }
}
