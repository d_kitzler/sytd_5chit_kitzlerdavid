package at.htl;

import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@NoArgsConstructor
public class ChocolateDecorator implements IIngredient
{
    @NonNull
    private IIngredient ingredient;

    private String description = "Schokolade";
    private  Integer prize = 9;

    public String getDescription() {
        return String.format("%s %s", ingredient.getDescription(), description);
    }

    public Integer getPrize()
    {
        return ingredient.getPrize() + prize;
    }
}
