package at.htl;

public interface ICoffeeFactory
{
    IIngredient createInstance();
}
