package at.htl;

import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@NoArgsConstructor
public class SoyDecorator implements IIngredient
{
    @NonNull
    private IIngredient ingredient;

    private String description = "Soja";
    private  Integer prize = 5;

    public String getDescription() {
        return String.format("%s %s", ingredient.getDescription(), description);
    }

    public Integer getPrize()
    {
        return ingredient.getPrize() + prize;
    }
}
