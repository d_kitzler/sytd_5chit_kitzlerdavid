package at.htl;

import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@NoArgsConstructor

public class MilkFoamDecorator implements IIngredient
{
    @NonNull
    private IIngredient ingredient;

    private String description = "Milchschaum";
    private  Integer prize = 3;

    public String getDescription() {
        return String.format("%s %s", ingredient.getDescription(), description);
    }

    public Integer getPrize()
    {
        return ingredient.getPrize() + prize;
    }
}
