package at.htl;

public class HouseMixFactory implements ICoffeeFactory
{
    @Override
    public IIngredient createInstance() {
        return new SoyDecorator(new HouseMix());
    }
}
