package at.htl;

public class IngridientBuilder
{
    private IIngredient ingredient;

    public IngridientBuilder init(IIngredient ingredient)
    {
        this.ingredient = ingredient;
        return this;
    }

    public IngridientBuilder addMilk()
    {
        ingredient = new MilkDecorator(ingredient);
        return this;
    }

    public IngridientBuilder addSoy()
    {
        ingredient = new SoyDecorator(ingredient);
        return this;
    }

    public IngridientBuilder addMilkFoam()
    {
        ingredient = new MilkFoamDecorator(ingredient);
        return this;
    }

    public IngridientBuilder addChocolate()
    {
        ingredient = new ChocolateDecorator(ingredient);
        return this;
    }

    public IIngredient create()
    {
        return ingredient;
    }
}
