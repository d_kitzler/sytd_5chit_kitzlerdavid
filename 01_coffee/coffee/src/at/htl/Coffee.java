package at.htl;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Coffee implements IIngredient
{
    private String description;
    private Integer prize;
}
