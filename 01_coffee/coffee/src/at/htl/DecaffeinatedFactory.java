package at.htl;

import java.util.zip.Deflater;

public class DecaffeinatedFactory implements ICoffeeFactory
{
    @Override
    public IIngredient createInstance() {
        return new MilkFoamDecorator(new Decaffeinated());
    }
}
