package at.htl;

public interface IIngredient
{
    String getDescription();
    Integer getPrize();
}
