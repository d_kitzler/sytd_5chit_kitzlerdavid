package at.htl;

public class DarkRoastedFactory implements ICoffeeFactory
{
    @Override
    public IIngredient createInstance() {
        return new MilkDecorator(new DarkRoasted());
    }
}
