package at.htl;

public class EspressoFactory implements ICoffeeFactory
{

    @Override
    public IIngredient createInstance() {
        return new ChocolateDecorator(new Espresso());
    }
}
