package at.htl;

public class Main {

    public static void main(String[] args)
    {
    	//WITHOUT FACTORY
	    IIngredient hausmischung = new ChocolateDecorator(new HouseMix());
	    IIngredient espresso = new ChocolateDecorator(new Espresso());
	    IIngredient decaiffeinated = new MilkDecorator(new Decaffeinated());
	    IIngredient darkRoasted = new SoyDecorator(new DarkRoasted());
	    System.out.println(darkRoasted.getPrize());

	    //FACTORY
	    IIngredient ef = new EspressoFactory().createInstance();
		System.out.println(ef.getPrize());

		//BUILDER
		IngridientBuilder ingridientBuilder = new IngridientBuilder();
		IIngredient ingredient = ingridientBuilder.init(new Espresso()).addMilk().create();

		System.out.println(ingredient.getPrize());
		System.out.println(ingredient.getDescription());
    }
}
