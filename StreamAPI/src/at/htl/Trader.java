package at.htl;

import lombok.Data;

import java.io.Serializable;

@Data
public class Trader implements Serializable
{
    private final String name;

    private final String city;

    public Trader(String name, String city)
    {
        this.name = name;
        this.city = city;
    }
}
