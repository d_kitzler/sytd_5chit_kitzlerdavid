package at.htl;

import at.htl.Trader;
import at.htl.Transaction;
import org.junit.Assert;
import org.junit.Test;

import javax.print.DocFlavor;
import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Collectors;;

public class TransactionUnitTest
{
     Trader raoul = new Trader("Raoul", "Cambridge");
     Trader mario = new Trader("Mario", "Milan");
     Trader alan = new Trader("Alan","Cambridge");
     Trader brian = new Trader("Brian","Madrid");

     List<Transaction> transactions = Arrays.asList(
             new Transaction(brian, 2011, 300),
             new Transaction(raoul, 2012, 1000),
             new Transaction(raoul, 2011, 400),
             new Transaction(mario, 2012, 700),
             new Transaction(mario, 2012, 700),
             new Transaction(alan, 2012, 950)
     );

     @Test
     public void TestTransaktion2011()
     {
          List<Transaction> expected = new ArrayList<>();
          expected.addAll(Arrays.asList(
                  new Transaction(brian, 2011, 300),
                  new Transaction(raoul, 2011,400)
          ));

          List<Transaction> result = transactions.parallelStream()
                  .filter((Transaction d)->d.getYear()==2011)
                  .sorted(Comparator.comparingInt((Transaction t)->t.getValue()))
                  .collect(Collectors.toList());

          Assert.assertEquals(expected,result);
     }

     @Test
     public void AllCitys()
     {
          List<String> expected = Arrays.asList("Madrid","Cambridge","Milan");

          List<String> result = transactions.parallelStream()
                  .map((Transaction t)->t.getTrader().getCity())
                  .distinct()
                  .collect(Collectors.toList());

          Assert.assertEquals(expected,result);
     }
     @Test
     public void TraderNameCambridge()
     {
          List<String> expected = Arrays.asList("Alan","Raoul");

          List<String> result = transactions.parallelStream()
                  .filter((Transaction t)->t.getTrader().getCity()=="Cambridge")
                  .map((Transaction t)->t.getTrader().getName())
                  .sorted()
                  .distinct()
                  .collect(Collectors.toList());

          Assert.assertEquals(expected,result);
     }
     @Test
     public void LowestTransactionValue()
     {
          Integer lowestValue = 300;
          Optional<Integer> result = transactions.parallelStream()
                  .map((Transaction t) -> t.getValue())
                  .reduce(Integer::min);
          Assert.assertEquals(lowestValue, result.get());
     }

     @Test
     public void TheLowestTransaction()
     {
          Transaction lowestExpected = new Transaction(brian, 2011, 300);

          Optional<Transaction> result = transactions.parallelStream()
                  .reduce((Transaction a, Transaction b) -> a.getValue() < b.getValue() ? a : b);
          Assert.assertEquals(lowestExpected, result.get());
     }

     @Test
     public void TraderNames()
     {
          String expected = "BrianRaoulMarioAlan";

          String result = transactions.parallelStream()
                  .map((Transaction t) -> t.getTrader().getName())
                  .distinct()
                  .reduce("", (a, b) -> a + b);
          Assert.assertEquals(expected, result);
     }

     @Test
     public void GroupCities()
     {
          HashMap<String, List<Transaction>> expected = new HashMap<String, List<Transaction>>();

          expected.put("Milan", Arrays.asList(new Transaction(mario, 2012, 700),
                  new Transaction(mario, 2012, 700)));

          expected.put("Madrid", Arrays.asList(new Transaction(brian, 2011, 300)));

          expected.put("Cambridge", Arrays.asList(new Transaction(raoul, 2012, 1000),
          new Transaction(raoul, 2011, 400), new Transaction(alan, 2012, 950)));

          Map<String, List<Transaction>> transactionsByCities = transactions.stream()
                  .collect(Collectors.groupingBy((Transaction t) -> t.getTrader().getCity()));

          Assert.assertEquals(expected, transactionsByCities);
     }

     enum Level {LOW, MEDIUM, HIGH}

     @Test
     public void GroupByRevenue()
     {
          HashMap<Level, List<Transaction>> expected = new HashMap<Level, List<Transaction>>();

          expected.put(Level.MEDIUM, Arrays.asList(new Transaction(brian, 2011, 300),
                  new Transaction(raoul, 2011, 400)));
          expected.put(Level.HIGH, Arrays.asList(new Transaction(raoul, 2012, 1000),
                  new Transaction(mario, 2012, 700),
                  new Transaction(mario, 2012, 700),
                  new Transaction(alan, 2012, 950)
          ));

          Map<Level, List<Transaction>> transactionsByLevel = transactions.stream().
                  collect(Collectors.groupingBy((Transaction t) -> {
                       if (t.getValue() <= 200)
                            return Level.LOW;
                       else if (t.getValue() <= 400)
                            return Level.MEDIUM;
                       else
                            return Level.HIGH;
                  }));

          Assert.assertEquals(expected, transactionsByLevel);
     }
}
