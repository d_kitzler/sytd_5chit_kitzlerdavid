package at.htl;

import lombok.Data;

import java.io.Serializable;

@Data
public class Transaction implements Serializable
{
    private final Trader trader;

    private final int year;

    private final int value;

    public Transaction(Trader trader, int year, int value) {
        this.trader = trader;
        this.year = year;
        this.value = value;
    }
}
