package at.htl;

import java.util.concurrent.ArrayBlockingQueue;

public class BlockedQueue extends ArrayBlockingQueue<Process> {
    private static BlockedQueue instance = new BlockedQueue();

    private BlockedQueue() {
        super(10);
    }

    public static BlockedQueue getInstance(){
        return instance;
    }
}
