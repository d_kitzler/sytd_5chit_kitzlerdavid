package at.htl;

public class Main {

    public static void main(String[] args) {
        Thread producer = new Thread(new WaitingProducer());
        producer.start();

        Thread waitingConsumer = new Thread(new WaitingConsumer());
        Thread processingConsumer = new Thread(new ProcessingConsumer());
        Thread abortedConsumer = new Thread(new AbortedConsumer());
        waitingConsumer.start();
        processingConsumer.start();
        abortedConsumer.start();
    }
}
