package at.htl;

import java.util.Random;

public class BlockedConsumer implements Runnable {
    @Override
    public void run() {
        try {
            while (true) {
                Process process = BlockedQueue.getInstance().take();
                Random r = new Random();
                if (r.nextInt(10) <= 4) {
                    ReadyQueue.getInstance().put(process);
                    process.switchState(EProcessState.READY);
                }
                else
                    BlockedQueue.getInstance().put(process);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
