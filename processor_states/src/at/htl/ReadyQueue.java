package at.htl;

import java.util.concurrent.ArrayBlockingQueue;

public class ReadyQueue extends ArrayBlockingQueue<Process> {
    private static ReadyQueue instance = new ReadyQueue();

    private ReadyQueue() {
        super(10);
    }

    public static ReadyQueue getInstance(){
        return instance;
    }
}
