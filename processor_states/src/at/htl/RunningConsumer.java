package at.htl;

import java.util.Random;

public class RunningConsumer implements Runnable {
    @Override
    public void run() {
        try {
            while (true) {

                Process process = RunningQueue.getInstance().take();
                System.out.println(process.getProcessID()+": processing");
                Random r = new Random();

                int result = r.nextInt(10);

                if (result <= 4) {
                    RunningQueue.getInstance().remove(process);
                    process.switchState(EProcessState.FINISHED);
                }

                else if (result > 4 && result <= 6) {
                    BlockedQueue.getInstance().put(process);
                    process.switchState(EProcessState.BLOCKED);
                }

                else if (result > 6 && result <= 8) {
                    ReadyQueue.getInstance().put(process);
                    process.switchState(EProcessState.READY);
                }

                else
                    RunningQueue.getInstance().put(process);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
