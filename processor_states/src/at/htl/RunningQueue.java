package at.htl;

import java.util.concurrent.ArrayBlockingQueue;

public class RunningQueue extends ArrayBlockingQueue<Process> {
    private static RunningQueue instance = new RunningQueue();

    private RunningQueue()
    {
        super(10);
    }

    public static RunningQueue getInstance()
    {
        return instance;
    }
}
