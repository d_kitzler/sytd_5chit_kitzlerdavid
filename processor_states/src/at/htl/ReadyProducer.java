package at.htl;

public class ReadyProducer implements Runnable {
    @Override
    public void run() {
        int counter = 0;
        ReadyQueue queue = ReadyQueue.getInstance();

        try {
            while (counter<=10) {
                queue.put(new Process("Task: "+counter++));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
