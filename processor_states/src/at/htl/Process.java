package at.htl;

import java.io.Serializable;

public class Process implements Serializable {
    private String processID;

    private EProcessState state;

    public Process(String processID) {
        this.processID = processID;
        state = EProcessState.READY;
    }

    public void switchState(EProcessState newState){
        System.out.println(processID+": switch from "+state+" to "+newState);
        state = newState;
    }
    public String getProcessID()
    {
        return processID;
    }
}
