package at.htl;

import java.util.Random;

public class ReadyConsumer implements Runnable {
    @Override
    public void run() {
        try {
            while (true) {
                Process process = ReadyQueue.getInstance().take();
                Random r = new Random();
                if (r.nextInt(10) <= 6) {
                    RunningQueue.getInstance().put(process);
                    process.switchState(EProcessState.RUNNING);
                }
                else
                    ReadyQueue.getInstance().put(process);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
