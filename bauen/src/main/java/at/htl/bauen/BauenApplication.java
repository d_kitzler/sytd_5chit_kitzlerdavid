package at.htl.bauen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BauenApplication {

    public static void main(String[] args) {
        SpringApplication.run(BauenApplication.class, args);
    }

}
