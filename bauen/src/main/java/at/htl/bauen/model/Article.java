package at.htl.bauen.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@ToString
@Data
@AllArgsConstructor
public class Article implements Serializable
{
    private String name;

    private String description;
}
