package at.htl.bauen.message.consumer;

import at.htl.bauen.model.Article;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class ArticleConsumer
{
    @Autowired
    private ObjectMapper objectMapper;

    private static final Logger log =
            LoggerFactory.getLogger(ArticleConsumer.class);


    @RabbitListener(queues = "htl.webshop.baustoffe")
    public void Baustoffelisten(String message) throws JsonProcessingException
    {
        Article article = objectMapper.readValue(message,Article.class);

        log.info(message);
    }

    @RabbitListener(queues = "htl.webshop.boeden")
    public void Boedenlisten(String message) throws JsonProcessingException
    {
        Article article = objectMapper.readValue(message,Article.class);

        log.info(message);
    }

    @RabbitListener(queues = "htl.webshop.fliesen")
    public void Fliesenlisten(String message) throws JsonProcessingException
    {
        Article article = objectMapper.readValue(message,Article.class);

        log.info(message);
    }

    @RabbitListener(queues = "htl.webshop.baustoffe")
    public void Repositorylisten(String message) throws JsonProcessingException
    {
        Article article = objectMapper.readValue(message,Article.class);

        log.info(message);
    }

}
