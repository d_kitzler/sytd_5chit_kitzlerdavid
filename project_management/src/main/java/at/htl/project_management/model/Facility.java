package at.htl.project_management.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class Facility implements Serializable
{
    private String facilityName;
}
