package at.htl.project_management.model;

import com.sun.org.apache.xpath.internal.operations.Bool;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class RequestFundingProject extends AProject
{
    private Boolean isFWFFunded;

    private Boolean isFFGFunded;

    private Boolean isEUFunded;
}
