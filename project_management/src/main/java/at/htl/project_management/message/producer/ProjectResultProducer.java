package at.htl.project_management.message.producer;

import at.htl.project_management.model.AProject;
import at.htl.project_management.model.EProjectState;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProjectResultProducer
{
    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void sendProjectResult(AProject aProject) throws JsonProcessingException {
        String qName ="";

        if(aProject.getProjectSstate() == EProjectState.APPROVED)
        {
            qName = "approved";
        }

        if(aProject.getProjectSstate() == EProjectState.CANCELD)
        {
            qName ="cancelled";
        }

        String message = objectMapper.writeValueAsString(aProject);

        rabbitTemplate.convertAndSend("htl.result.exchange","accepted",message);
    }
}
