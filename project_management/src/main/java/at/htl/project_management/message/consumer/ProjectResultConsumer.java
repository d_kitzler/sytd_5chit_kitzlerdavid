package at.htl.project_management.message.consumer;

import at.htl.project_management.message.producer.ProjectResultProducer;
import at.htl.project_management.model.AProject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProjectResultConsumer {

    private static Logger log = LoggerFactory.getLogger(ProjectResultConsumer.class);

    @Autowired
    private ObjectMapper objectMapper;

    @RabbitListener(queues = "htl.result.accepted")
    public void listenProjectResultCancelled(String message) throws JsonProcessingException {
        AProject project = objectMapper.readValue(message, AProject.class);
        log.info("sdsd Received Project: {}",project.toString());
    }

    @RabbitListener(queues = "htl.result.cancelled")
    public void listenProjectResultAccepted(String message) throws JsonProcessingException {
        AProject project = objectMapper.readValue(message, AProject.class);
        log.info("Received Project: {}",project.toString());
    }


}
