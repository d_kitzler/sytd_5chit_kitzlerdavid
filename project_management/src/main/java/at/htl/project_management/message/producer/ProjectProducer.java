package at.htl.project_management.message.producer;

import at.htl.project_management.message.consumer.ProjectConsumer;
import at.htl.project_management.model.AProject;
import at.htl.project_management.model.EProjectState;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class ProjectProducer
{

    private static final Logger logger = LoggerFactory.getLogger(ProjectConsumer.class);

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void sendProject(AProject project) throws JsonProcessingException {
        logger.info("message in Producer");
        Random randy = new Random();

        if(randy.nextInt(6) == 1)
        {
            project.setProjectSstate(EProjectState.CANCELD);
        }

        else
        {
            project.setProjectSstate(EProjectState.APPROVED);
        }

        String message = objectMapper.writeValueAsString(project);
        rabbitTemplate.convertAndSend("htl.project.exchange","chemie",message);

    }
}
