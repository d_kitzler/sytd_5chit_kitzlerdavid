package at.htl.project_management.service;

import at.htl.project_management.message.consumer.ProjectConsumer;
import at.htl.project_management.message.producer.ProjectProducer;
import at.htl.project_management.model.AProject;
import at.htl.project_management.model.EProjectState;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path="/project")
public class ProjectRessource
{
    private static final Logger logger = LoggerFactory.getLogger(ProjectRessource.class);

    @Autowired
    private ProjectProducer projectProducer;



    @PostMapping(path = "/approvement/init")
    @ResponseStatus(HttpStatus.OK)
    public void approveProject(@RequestBody AProject project) throws JsonProcessingException {
        logger.info("service arrived");
        if(project.getProjectSstate() == EProjectState.CREATED)
        {
            projectProducer.sendProject(project);
        }
    }
}
