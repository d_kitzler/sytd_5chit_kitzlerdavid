package at.htl;

import java.util.concurrent.Semaphore;

public class Student implements Runnable
{
    private Semaphore semS, semB;

    public Student(Semaphore semS, Semaphore semB)
    {
        this.semB = semB;
        this.semS = semS;
    }

    @Override
    public void run()
    {
        try {
            while(true){
            semS.release();
            semB.acquire();
            insight();
            } }
        catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public void insight()
    {
        System.out.println("Student insight");
    }
}
