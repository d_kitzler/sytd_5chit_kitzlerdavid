package at.htl;

import java.util.concurrent.Semaphore;

public class Main {

    public static void main(String[] args)
    {
	// write your code here

        Semaphore semS = new Semaphore(0);
        Semaphore semT = new Semaphore(1);
        Semaphore semB = new Semaphore(0);

        Thread threadStudent = new Thread(new Student(semS,semB));
        Thread threadAssistent = new Thread(new Assistent(semS,semB, semT));

        threadStudent.start();
        threadAssistent.start();
    }
}
