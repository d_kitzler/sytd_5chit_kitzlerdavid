package at.htl;

import java.util.concurrent.Semaphore;

public class Assistent implements Runnable
{
    private Semaphore semS, semB, semT;

    public Assistent(Semaphore semS, Semaphore semB, Semaphore semT)
    {
        this.semB = semB;
        this.semS = semS;
        this.semT = semT;
    }

    @Override
    public void run() {
        while (true) {
            try {
                semS.acquire();


                semT.acquire();
                fetchExam();
                semT.release();
                insight();
                semB.release();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void insight()
    {
        System.out.println("Assistent insight");
    }

    public void fetchExam()
    {
        System.out.println("Assistent fetch exam");
    }
}
