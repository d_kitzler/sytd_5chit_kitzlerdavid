package at.htl;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@AllArgsConstructor
@Data
public class Dimension implements Serializable
{
    private int width;

    private int breadth;

    private int height;
}
