package at.htl;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class FunctionUtil
{
    public List<Integer> evaluateFunction(List<Integer> values, Function<Integer, Integer> function)
    {
        List<Integer> sum = new ArrayList<>();

        for(Integer v : values)
        {
            sum.add(function.apply(v));
        }

        return sum;
    }
}
