package at.htl;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Ware implements Serializable
{
    private String label;

    private ECondition condition;

    private int weight;

    private Dimension dimension;
}
