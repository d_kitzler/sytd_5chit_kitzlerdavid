package at.htl;

import java.awt.event.WindowAdapter;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class ProductionProcess
{
    public List<Ware> evaluate (List<Ware> wares, Predicate<Ware> predicate)
    {
        List<Ware> wareList = new ArrayList<>();

        for(Ware ware : wares)
        {
            if(predicate.test(ware))
                wareList.add(ware);
        }

        return wareList;
    }
}
