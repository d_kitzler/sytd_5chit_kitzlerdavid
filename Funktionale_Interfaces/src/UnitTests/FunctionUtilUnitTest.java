package UnitTests;

import at.htl.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.util.function.Function;
import java.util.function.Predicate;
import org.junit.Assert;
import org.junit.Test;
public class FunctionUtilUnitTest
{
    FunctionUtil functionUtil = new FunctionUtil();
    List<Integer> numbers = Arrays.asList(0,1,2,3,4,5,6,7,8,9);
    Function<Integer,Integer> PlusOne = (Integer i) -> i+1;
    Function<Integer,Integer> MulitplicateByTwo = (Integer i) -> i*2;

    @Test
    public void PlusOneTest()
    {
        List<Integer> expected = new ArrayList<>();
        for (int i =0; i< 10; i++)
        {
            expected.add(i+1);
        }

        List<Integer> result = functionUtil.evaluateFunction(numbers,PlusOne);
        Assert.assertEquals(expected,result);
    }

    @Test
    public void MulitplicateByTwoTest()
    {
        List<Integer> expected = new ArrayList<>();
        for (int i =0; i< 10; i++)
        {
            expected.add(i*2);
        }

        List<Integer> result = functionUtil.evaluateFunction(numbers,MulitplicateByTwo);
        Assert.assertEquals(expected,result);
    }

    @Test
    public void MulitplicateByTwoAndPlusOneTest()
    {
        List<Integer> expected = new ArrayList<>();
        for (int i =0; i< 10; i++)
        {
            expected.add((i*2)+1);
        }

        List<Integer> result = functionUtil.evaluateFunction(numbers,MulitplicateByTwo.andThen(PlusOne));
        Assert.assertEquals(expected,result);
    }
}
