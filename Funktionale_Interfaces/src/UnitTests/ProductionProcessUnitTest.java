package UnitTests;

import at.htl.*;
import java.util.List;
import java.util.Arrays;
import java.util.function.Predicate;
import org.junit.Assert;
import org.junit.Test;

public class ProductionProcessUnitTest
{
    List<Ware> wares = Arrays.asList(
            new Ware("Kaffee", ECondition.USED, 23, new Dimension(2, 1, 9)),
            new Ware("Schnitzel", ECondition.DAMAGED, 10, new Dimension(5, 2, 10)),
            new Ware("Lasagne", ECondition.NEW, 19, new Dimension(3, 9, 2)),
            new Ware("Eistee", ECondition.NEW, 36, new Dimension(4, 7, 6))
            );

    Predicate<Ware> conditionNEW = (Ware w) -> w.getCondition() == ECondition.NEW;
    Predicate<Ware> under20kg = (Ware w) -> w.getWeight() < 20;

    ProductionProcess productionProcess = new ProductionProcess();

    @Test
    public void OnlyConditionNew()
    {
        List<Ware> filtered = productionProcess.evaluate(wares, conditionNEW);
        Assert.assertEquals(2,filtered.size());
    }

    @Test
    public void OnlyUnder20kgAndConditionNew()
    {
        List<Ware> filtered = productionProcess.evaluate(wares, conditionNEW.and(under20kg));
        Assert.assertEquals(1,filtered.size());
    }

}
