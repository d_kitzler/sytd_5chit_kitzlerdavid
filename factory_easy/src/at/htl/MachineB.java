package at.htl;

import java.util.concurrent.Semaphore;

public class MachineB implements Runnable {

    private Semaphore semB;

    private Semaphore semConveyorBelt;

    public MachineB(Semaphore semB, Semaphore semConveyorBelt) {
        this.semB = semB;
        this.semConveyorBelt = semConveyorBelt;
    }

    @Override
    public void run() {
        try {
            while (true) {
                semB.acquire();
                process();
                semConveyorBelt.release();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public void process() throws InterruptedException {
        System.out.println("MachineB process");
        Thread.sleep(500);
    }
}
