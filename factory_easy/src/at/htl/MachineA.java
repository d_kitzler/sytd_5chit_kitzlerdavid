package at.htl;

import java.util.concurrent.Semaphore;

public class MachineA implements Runnable {

    private Semaphore semA;

    private Semaphore semConveyorBelt;

    public MachineA(Semaphore semA, Semaphore semConveyorBelt) {
        this.semA = semA;
        this.semConveyorBelt = semConveyorBelt;
    }

    @Override
    public void run() {
        try {
            while (true) {
                semA.acquire();
                process();
                semConveyorBelt.release();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void process() throws InterruptedException {
        System.out.println("MachineA process");
        Thread.sleep(500);
    }
}
