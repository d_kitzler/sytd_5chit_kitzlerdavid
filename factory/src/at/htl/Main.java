package at.htl;

import java.util.concurrent.Semaphore;

public class Main {

    public static void main(String[] args) {
        Semaphore semA = new Semaphore(0);
        Semaphore semB = new Semaphore(0);
        Semaphore semConveyorBelt = new Semaphore(3);

        Thread threadMachineB = new Thread(new MachineB(semB, semConveyorBelt));
        Thread threadMachineA = new Thread(new MachineA(semA, semConveyorBelt));
        Thread threadConveyorBelt = new Thread(new ConveyorBelt(semA, semB, semConveyorBelt));

        threadConveyorBelt.start();
        threadMachineA.start();
        threadMachineB.start();
    }
}
